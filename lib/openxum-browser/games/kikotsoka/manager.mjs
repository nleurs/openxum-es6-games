"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Kikotsoka from '../../../openxum-core/games/kikotsoka/index.mjs';

class Manager extends OpenXum.Manager {
  constructor(e, g, o, s, w) {
    super(e, g, o, s, w);
    this.that(this);
  }

  build_move() {
    return new Kikotsoka.Move();
  }

  get_current_color() {
    return this.engine().current_color() === Kikotsoka.Color.BLACK ? 'Black' : 'White';
  }

  static get_name() {
    return 'kikotsoka';
  }

  get_winner_color() {
    return this.engine().winner_is() === Kikotsoka.Color.BLACK ? 'black' : 'white';
  }

  process_move() {
  }
}

export default Manager;